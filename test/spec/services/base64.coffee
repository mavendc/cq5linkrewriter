'use strict'

describe 'Service: Base64', () ->

  # load the service's module
  beforeEach module 'linkRewriterApp'

  # instantiate service
  Base64 = {}
  beforeEach inject (_Base64_) ->
    Base64 = _Base64_

  it 'should do something', () ->
    expect(!!Base64).toBe true
