'use strict'

describe 'Service: Outputlog', () ->

  # load the service's module
  beforeEach module 'linkRewriterApp'

  # instantiate service
  Outputlog = {}
  beforeEach inject (_Outputlog_) ->
    Outputlog = _Outputlog_

  it 'should do something', () ->
    expect(!!Outputlog).toBe true
