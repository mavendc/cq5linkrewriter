'use strict'

angular.module('linkRewriterApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ui.bootstrap',
  'ui.utils'
])
  .config ($routeProvider, $sceProvider, $locationProvider) ->

    $sceProvider.enabled false

    $routeProvider
      .when '/',
        templateUrl: 'partials/main'
        controller: 'MainCtrl'
      
      .when '/query',
        templateUrl: 'partials/query'
        controller: 'QueryCtrl'
      .when '/review',
        templateUrl: 'partials/review'
        controller: 'ReviewCtrl'
      .when '/map',
        templateUrl: 'partials/map'
        controller: 'MapCtrl'
      .when '/finalize',
        templateUrl: 'partials/finalize'
        controller: 'FinalizeCtrl'
      .otherwise
        redirectTo: '/'

    $locationProvider.html5Mode true