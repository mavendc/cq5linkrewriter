'use strict'

angular.module('linkRewriterApp').service 'Outputlog', ($log) ->
  @log = (arg) ->
    $log.info arg
  return
