'use strict'

angular.module('linkRewriterApp').factory "Auth", ["Base64","$cookieStore","$http", (Base64, $cookieStore, $http) ->

    return (
      setCredentials: (username, password) ->
        encoded = Base64.encode(username + ":" + password)
        $http.defaults.headers.common = {};
        $http.defaults.headers.common.Authorization = "Basic " + encoded
        $http.defaults.headers.common["Authorization"] ="Basic " + encoded
        delete $http.defaults.headers.common['X-Requested-With'];
        return

    )
]