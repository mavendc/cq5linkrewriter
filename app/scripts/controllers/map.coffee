'use strict'

angular.module('linkRewriterApp').controller 'MapCtrl', ($scope, $rootScope,  $http, Auth) ->

  $scope.init = () ->
    $scope.loadedNodes = []
    $scope.run = true
    $scope.alerts = []

  do $scope.init

  $scope.abortQueries = () ->
    $scope.run = false
    $scope.loadedNodes = []
    $scope.alerts.push {msg: "Queries aborted", type : "info"}

  $scope.saveResults = () ->
    $rootScope.loadedNodes = $scope.nodesByGroup
    $scope.alerts.push {msg: "Mapped node groups saved to root scope and cookies", type : "success"}

  $scope.nodesByGroup = _($scope.nodes).groupBy('key').map( (array, key) ->
    {key : key , totalNodes : array, nodesProcessed : [] , nodesFailedProcessed : [] }).value()

  $scope.loadTextNodes = () ->
    Auth.setCredentials $scope.config?.serverConfig?.user, $scope.config?.serverConfig?.pwd
    _($scope.nodesByGroup).forEach (group) ->
      if $scope.run
        $scope.processGroup group

  $scope.processGroup = (group) ->
    $scope.nodesTotal = group?.totalNodes
    _(group?.totalNodes).forEach (node) ->
      if $scope.run
        rq = $http
          method: "GET"
          url: "#{$scope.config.serverConfig.host}#{node.path}.json"
        rq.success (data) ->
          node.data = data
          group.nodesProcessed.push node
          console.log "#{node.path}"
        rq.error (data) ->
          group.nodesFailedProcessed push node
