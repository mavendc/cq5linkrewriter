'use strict'

angular.module('linkRewriterApp').controller 'ReviewCtrl', ($scope, $http,  $sanitize) ->

  $scope.init = () ->
    _($scope.loadedNodes).forEach (group) ->
      _(group?.nodesProcessed).forEach (node) ->
        node?.data?.oldText = node?.data?.text
        node?.data?.oldLinkURL = node?.data?.linkURL
    console.log $scope.loadedNodes
    $scope.alerts = []

  do $scope.init

  $scope.saveResults = () ->
    $scope.alerts.push {msg: "Saved modified text toroot scope and cookies", type : "success"}

  $scope.process1to1 = () ->
    $scope.alerts.push {msg: "Processed 1 to 1 link rewrites", type : "success"}

  $scope.processFuzzy = () ->
    $scope.alerts.push {msg: "Processed fuzzy link rewrites", type : "success"}



