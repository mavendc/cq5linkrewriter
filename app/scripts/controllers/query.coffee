'use strict'

angular.module('linkRewriterApp').controller 'QueryCtrl', ($scope, $rootScope, $http, Auth) ->

  $scope.init = () ->
    $scope.alerts = []
    $scope.totalSites = []
    $scope.queryPrefix = #append site key to this
    $scope.loading = false;
    $scope.siteCount = 0
    $scope.totalSiteCount =0
    $scope.progress = 0
    $scope.nodes = []
    $scope.run = true

  do $scope.init

  $scope.abortQueries = () ->
    $scope.init()
    $scope.run = false

  $scope.processResults = (results) ->
    _(results).forEach (node) ->
      if $scope.run
        pageEnd = node.path?.lastIndexOf("/jcr:content")
        if pageEnd > -1
          node.page = node.path.substring(0,pageEnd)
        else
          node.page = ""
        $scope.nodes.push node

  $scope.saveNodes = () ->
    $rootScope.nodes = $scope.nodes
    $scope.alerts.push {msg: "#{$scope.nodes.length} nodes saved successfully (to root scope and cookies)!", type : "success"}

  $scope.loadSites = () ->
    $scope.alerts = []
    $scope.totalSites = (_($scope.config.sites).map (a, b) -> { key : b , enabled : a } ).filter('enabled')
    $scope.totalSiteCount = $scope.totalSites.value().length
    $scope.totalSites.forEach (site) ->
      if $scope.run
        $scope.loadSite site?.key

  $scope.loadSite = (key) ->
    if $scope.run
      $scope.alerts.push {msg: "Executing query on site #{key}....", type : "info"}
      $scope.loading = true
      Auth.setCredentials $scope.config?.serverConfig?.user, $scope.config?.serverConfig?.pwd

      queryPrefix = "/jcr:root/content/"
      term = $scope.config.serverConfig.queryTerm
      stmt2 = queryPrefix + key + "//*[jcr:contains(@text,'#{term}.com') or jcr:contains(@linkURL,'#{term}.com')]"

      $scope.progress = ($scope.siteCount / $scope.totalSiteCount) * 100

      rq = $http
        method: "GET"
        params :
          _charset : "utf-8"
          type : "xpath"
          stmt : stmt2
          showResults : true
        url: "#{$scope.config.serverConfig.host}/crx/de/query.jsp"

      rq.success (data) ->
        $scope.loading = false
        $scope.siteCount +=1
        resultType = if data?.results?.length <1 then 'danger' else 'success'
        $scope.alerts.push {msg: "Query successfuly executed on  #{key}, with #{data?.results?.length} results", type : resultType}
        _(data?.results).forEach (node) ->_(node).assign {key : key}
        $scope.progress = ($scope.siteCount / $scope.totalSiteCount) * 100
        $scope.processResults data?.results
