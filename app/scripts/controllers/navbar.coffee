'use strict'

angular.module('linkRewriterApp')
  .controller 'NavbarCtrl', ($scope, $location) ->
    $scope.menu = [
      { title: 'Home' , link: '/' },
      { title: 'Query' , link: '/query' }
      { title : 'Map', link: '/map'}
      { title : 'Review', link: '/review'}
      { title : 'Finalize', link: '/finalize'}
    ]

    
    $scope.isActive = (route) ->
      route is $location.path()