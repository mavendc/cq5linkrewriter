'use strict'

angular.module('linkRewriterApp').controller 'RewriterCtrl', ($scope, $rootScope, Outputlog) ->
  $scope.init = () ->

    $scope.serverConfig = {
      title : "Server Configuration"
      host : "http://dqa.pennwell.net"
      user : "wcmsposter"
      pwd : "Pennwell*8"
      queryTerm : "omeda"
      sitesRoot : "http://mavendc-utils.s3.amazonaws.com/pennwell/urlrewrites.json"
    }

    $scope.actionConfig = {
      title : "Execution Configuration"
      isDryRun : true
      activatePages : false
      doFuzzyMatch : true
    }

    $scope.queryParams = [
      _charset : "utf-8",
        type : "xpath",
          stmt : "//*[jcr:contains(@text,'omeda.com') or jcr:contains(@linkURL,'omeda.com')]",
            showResults : true
    ]
    $scope.sites =
    {
      'aiwf' : false
      'ap' : false
      'avi' : false
      'avieuro' : false
      'bow' : false
      'brasilevents' : false
      'cg' : false
      'cge' : false
      'cim' : false
      'confab' : false
      'cospp' : false
      'de' : false
      'diq' : false
      'dof' : false
      'dot' : false
      'dt' : false
      'dta' : false
      'dtb' : false
      'eiq' : false
      'eiq-2' : false
      'elp' : false
      'elpexec' : false
      'ems' : false
      'emst' : false
      'fa' : false
      'fdic' : false
      'geometrixx' : true
      'fe' : false
      'hvaf' : false
      'hvb' : false
      'hve' : false
      'hvi' : false
      'hvr' : false
      'hydro' : false
      'ils' : false
      'iww' : false
      'leds' : false
      'ledshow' : false
      'lfw' : false
      'lois' : false
      'lpms' : false
      'lw' : false
      'mae' : false
      'mapsearch' : false
      'nha' : false
      'npe' : false
      'npi' : false
      'oa' : false
      'ogfj' : false
      'ogj' : false
      'ogpe' : false
      'oiq' : false
      'ome' : false
      'os' : false
      'oshot' : false
      'owa' : false
      'pe' : false
      'pei' : false
      'pej' : false
      'pennwell-corporate' : false
      'pennwellqa' : false
      'pga' : false
      'pgaf' : false
      'pgaff' : false
      'pgb' : false
      'pge' : false
      'pgi' : false
      'pgica' : false
      'pgiff' : false
      'pgme' : false
      'pgw' : false
      'pgweek' : false
      'pnec' : false
      'ppg' : false
      'ptj' : false
      'pvw' : false
      'pwi' : false
      'rdh' : false
      'rewa' : false
      'rewaf' : false
      'rewe' : false
      'rewi' : false
      'rewna' : false
      'rp' : false
      'sho' : false
      'sib' : false
      'sil' : false
      'sile' : false
      'silj' : false
      'spg' : false
      'sr' : false
      'sst' : false
      'stf' : false
      'stm' : false
      'top' : false
      'uaetd' : false
      'uog' : false
      'uor' : false
      'up' : false
      'upbadcopy' : false
      'upc' : false
      'vsd' : true
      'wmw' : false
      'ww' : false
      'wwme' : false
    }


    $rootScope.config = $scope

  $scope.removeQueryParam = () ->
    $scope.queryParams.pop()

  $scope.addQueryParam = (paramKey) ->
    obj = {}
    obj[paramKey] = ""
    $scope.queryParams.push obj

  $scope.alerts = []

  $scope.saveConfig = () ->
    $scope.alerts.push msg: "Configuration saved!"
    $rootScope.config = $scope

  $scope.resetConfig = () ->
    $scope.init()
    $scope.alerts.push msg: "Configuration reset!"

  $scope.closeAlert = (index) ->
    $scope.alerts.splice index, 1



